package DictionariOperations;

import ro.wantsome.Utils;
import java.io.IOException;
import java.util.*;


public class AnagramsDictionaryOperation implements DictionaryOperation {
  public List<String> allLines = new ArrayList<>();

    public AnagramsDictionaryOperation(List<String> allLines) {
        this.allLines = allLines;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Anagrams");
        //System.out.println("dimensiunea allline " + allLines.size()); verificam cate linii are dictionarul inainte de a scoate duplicatele din el
        HashMap<String, List<String>> map = new HashMap<>();
        List<String> allfinallines = new ArrayList<>();
        allfinallines = Utils.removeDuplicateList(allLines);
        //System.out.println("dimensiunea allfinallines " + allfinallines.size()); verificam cate linii are dictionarul dupa ce am scos duplicatele din el
        // parcurgem tot dictionarul dupa ce am scos duplicatele din el
        for (int i = 0; i < allfinallines.size(); i++) {

            // convertim in array de charuri
            // sortam array-ul
            // reconvertim in string array-ul
            String word = allfinallines.get(i);
            String newWord = Utils.sortLetters(word);

            // dupa ce am sortat
            // obtinem hashcodul din stringuri
            if (map.containsKey(newWord)) {

                map.get(newWord).add(word);
            } else {

                // adaugam intr-o lista cuvintele care au un anumit hashcod
                List<String> words = new ArrayList<>();
                words.add(word);
                map.put(newWord, words);
            }
        }

        // afisam toate perechile de cuvinte care au dimensiunea mai mare decat 1
        for (String s : map.keySet()) {
            List<String> values = map.get(s);
            if (values.size() > 1) {
                System.out.print(values + "\n\t");
            }
        }
    }
}
