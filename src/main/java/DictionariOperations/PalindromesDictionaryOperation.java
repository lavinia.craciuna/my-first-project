package DictionariOperations;

import ro.wantsome.Utils;

import java.io.IOException;
import java.util.*;

public class PalindromesDictionaryOperation implements DictionaryOperation {
    private Set<String> wordSet;

    public PalindromesDictionaryOperation(Set<String> wordSet) {
        this.wordSet = wordSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Palindromes");
        Set<String> palindrom = Utils.findPalindromes(wordSet);
        List<String> sortPalindromes=Utils.sortSet(palindrom);
        for (String line : sortPalindromes)
            System.out.println(line);
    }
}
