package DictionariOperations;

import java.io.IOException;

public interface DictionaryOperation {
    void run() throws IOException;
}
