package DictionariOperations;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class SearchDictionaryOperation implements DictionaryOperation {
   private BufferedReader in;
   private Set<String> wordSet;
   public SearchDictionaryOperation(Set<String> wordSet, BufferedReader in){
       this.in=in;
       this.wordSet=wordSet;
   }
    @Override
    public void run() throws IOException {
        System.out.println("Searching");
        String userGivenWord=in.readLine();
        if ("".equals(userGivenWord))//daca dam un enter vom intrerupe bucla
            return;
        Set<String> resultedWords = Utils.findWordsThatContain(wordSet, userGivenWord);
        List<String> sortedWords=Utils.sortSet(resultedWords);

        for (String line : sortedWords) {
            System.out.println(line);
        }
    }
}
