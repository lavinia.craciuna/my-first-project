package FileReader;

import java.io.IOException;
import java.util.*;

public interface InputFileReader {
    List<String> readfile(String imput) throws IOException;
}
