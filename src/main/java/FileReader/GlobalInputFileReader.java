package FileReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class GlobalInputFileReader implements InputFileReader {
    @Override
    public List<String> readfile(String imput) throws IOException {
        return Files.readAllLines(Paths.get(imput));
    }
}
