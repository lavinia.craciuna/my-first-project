package ro.wantsome;

import DictionariOperations.*;
import FileReader.GlobalInputFileReader;
import FileReader.InputFileReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        List<String> allLines = new ArrayList<>();
        //InputFileReader inputFileReader=new ResourceInputFileReader();
        InputFileReader inputFileReader=new GlobalInputFileReader();
        allLines = inputFileReader.readfile("C:\\code\\myfirstproject\\src\\main\\resources\\dex.txt");
        Set<String> wordSet = Utils.removeDuplicate(allLines);

        //varianta 2 de a pune cuvintele in set
        // for(String line:allLines){
        //  wordSet.add(line);
        // }
        // Scanner in = new Scanner(System.in);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Menu: \n\t" +
                    "1.Search\n\t" +
                    "2.Palindromes\n\t" +
                    "3.Anagrams\n\t" +
                    "4.RandomWord\n\t" +
                    "0.Exiting");
            String userMenuSelection = in.readLine();

            if (userMenuSelection.equals("0")) {
                System.out.println("Exiting...");
                break;
            }
            DictionaryOperation operation=null;
            switch (userMenuSelection) {
                case "1":
                    operation=new SearchDictionaryOperation(wordSet,in);
                    break;
                case "2":
                    operation=new PalindromesDictionaryOperation(wordSet);
                    break;
                case "3":
                    operation=new AnagramsDictionaryOperation(allLines);
                    break;
                case"4":
                    operation=new RandomWordDictionaryOperation(wordSet);
                    break;
                default:
                    System.out.println("Please select a valid option");
            }
            if(operation!=null){
                operation.run();
            }
        }
    }
}
