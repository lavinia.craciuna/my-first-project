package ro.wantsome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class Utils {
    /**
     * Can read a given file from the resources folder
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = Main.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }

    /**
     * Can read a given file in the working directory
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromSourceFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }

    public static Set<String> removeDuplicate(List<String> allLines) {
        Set<String> wordSet = new HashSet<>();
        //varianta 1 de a pune cuvintele in set
        for (int i = 0; i < allLines.size(); i++) {
            wordSet.add(allLines.get(i));
        }
        return wordSet;
    }

    public static List<String> removeDuplicateList(List<String> allLines) {
        List<String> wordList = new ArrayList<>();
        Set<String> wordSet = new HashSet<>();
        for (int i = 0; i < allLines.size(); i++) {
            wordSet.add(allLines.get(i));
        }
        wordList.addAll(wordSet);
        return wordList;
    }

    public static Set<String> findWordsThatContain(Set<String> wordSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordSet) {
            if (line.contains(word))
                result.add(line);

        }
        return result;
    }

    public static Set<String> findPalindromes(Set<String> wordsList) {
        Set<String> result = new HashSet<>();
        if (wordsList == null) {
            System.out.println("Warning null parameter in findPalindroms");
            return result;
        }
        for (String line : wordsList) {

            StringBuilder reverseWord = new StringBuilder(line).reverse();
            if (line.equals(reverseWord.toString())) {
                result.add(line);
            }
        }
        return result;
    }

    public static String sortLetters(String word) {
        char[] letters = word.toCharArray();
        Arrays.sort(letters);
        return new String(letters);
    }

    public static List<String> sortSet(Set<String> unsortedSet) {

        List<String> result = new ArrayList<>(unsortedSet);
        Collections.sort(result);
        return result;
    }

    public static String getRandomWord(Set<String> wordSet) {
        //folosim Clasa Random pentru a extrage un cuvant dintr-un set
        Random random = new Random();

        return getRandomWordFromSet(wordSet, random);

    }

    public static String getRandomWordFromSet(Set<String> wordSet, Random random) {
        List<String> wordList = new ArrayList<>(wordSet);
        int index = random.nextInt(wordList.size());
        return wordList.get(index);
        // ne da un numar de la 0 pana la ultimul element din lista return wordList.get(index);
    }

    public static List<String> findAnagrams(List<String> allLines) {
        HashMap<String, List<String>> map = new HashMap<>();
        List<String> allfinallines = new ArrayList<>();
        allfinallines = Utils.removeDuplicateList(allLines);
        //System.out.println("dimensiunea allfinallines " + allfinallines.size()); verificam cate linii are dictionarul dupa ce am scos duplicatele din el
        // parcurgem tot dictionarul dupa ce am scos duplicatele din el
        for (int i = 0; i < allfinallines.size(); i++) {

            // convertim in array de charuri
            // sortam array-ul
            // reconvertim in string array-ul
            String word = allfinallines.get(i);
            String newWord = Utils.sortLetters(word);

            // dupa ce am sortat
            // obtinem hashcodul din stringuri
            if (map.containsKey(newWord)) {

                map.get(newWord).add(word);
            } else {

                // adaugam intr-o lista cuvintele care au un anumit hashcod
                List<String> words = new ArrayList<>();
                words.add(word);
                map.put(newWord, words);
            }
        }
        List<String> anagrams = new ArrayList<>();

        // afisam toate perechile de cuvinte care au dimensiunea mai mare decat 1
        for (String s : map.keySet()) {
            List<String> values = map.get(s);
            if (values.size() > 1) {
                System.out.print(values + "\n\t");
                anagrams.addAll(values);
            }
        }
        return anagrams;
    }
}