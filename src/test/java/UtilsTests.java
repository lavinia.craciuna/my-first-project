import DictionariOperations.AnagramsDictionaryOperation;
import org.junit.Assert;
import org.junit.Test;
import ro.wantsome.Utils;

import java.util.*;

public class UtilsTests {
    @Test
    public void testPalindrom() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("cojoc");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertTrue(palindromes.contains("cojoc"));
    }

    @Test
    public void testPalindromFinder_FindNothing() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("asa");
        inputSet.add("ata");
        inputSet.add("cojoc");
        inputSet.add("penar");

        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertEquals(3, palindromes.size());
    }
    @Test
    public void testPalindromFinder_FindNothing_IsResilient() {

        Set<String> palindromes = Utils.findPalindromes(null);
        Assert.assertEquals(0, palindromes.size());
    }
    @Test
    public void testSortLetter(){
        String initial="dcba";
        String rezultat = Utils.sortLetters(initial);
        Assert.assertEquals("abcd",rezultat);
    }
    @Test
    public void testRandomWordFinder(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("1");
        inputSet.add("2");
        inputSet.add("3");
        inputSet.add("4");
        String randomWord=Utils.getRandomWordFromSet(inputSet,new Random (1));
        Assert.assertEquals("3",randomWord);
    }
    @Test
    public void testFindAnagrame(){
        List<String> wordList=new ArrayList<>();
        wordList.add("cat");
        wordList.add("act");
        wordList.add("caruta");
        wordList.add("tac");
        wordList.add("cat");
        wordList.add("c");
        List <String>anagrams =Utils.findAnagrams(wordList);
        Assert.assertEquals("[act, cat, tac]",anagrams.toString());
    }
}
